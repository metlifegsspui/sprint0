/*global module:false*/
module.exports = function(grunt) {

	var globalConfig = {
		inputDirectory : 'src/components',
		outputDirectory : 'common'
	};

	// Project configuration.
	grunt
		.initConfig({
			// Metadata.
			pkg : grunt.file.readJSON('package.json'),
			globalConfig: globalConfig,
			banner : '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - '
					+ '<%= grunt.template.today("yyyy-mm-dd") %>\n'
					+ '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>'
					+ '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;'
					+ ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
			compass : {
				dist : {
					options : {
						sassDir : "<%= globalConfig.inputDirectory %>/**",
						cssDir : "<%= globalConfig.inputDirectory %>/css",
						trace : true,
						force: true
					}
				}
			},

			cssmin : {
				combine : {
					files : {
						"<%= globalConfig.inputDirectory %>/output.min.css" : [ "<%= globalConfig.inputDirectory %>/**/*.css" ]
					}
				},
				minify : {
					expand : true,
					cwd : "<%= globalConfig.inputDirectory %>/",
					src : [ 'output.min.css' ],
					dest : "<%= globalConfig.inputDirectory %>",
					ext : '.min.css'
				}
			},

			mkdir : {
				css: {
					options: {
						create: ["<%= globalConfig.inputDirectory %>/_css"]
					}
				}
			},

			clean : {
				css : ["<%= globalConfig.inputDirectory %>/css", "<%= globalConfig.inputDirectory %>/**/*.css", ".sass-cache" ]
			},

			watch : {
				scripts : {
					files : [ '**/*.scss' ],
					tasks : [ 'styles' ],
					options : {
						spawn : false,
					}
				}
			}
		});

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-mkdir');

	// Default task.
	grunt.registerTask('default', [ 'styles', 'watch' ]);
	grunt.registerTask('styles', [ 'clean:css', 'compass', 'compass', 'cssmin', 'clean:css' ]);

};
