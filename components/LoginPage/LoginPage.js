/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import withStyles from '../../decorators/withStyles';
import Link from '../global/Link';

class LoginPage extends Component {

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  };

  render() {
    const title = 'Customer Login';
    const usernamelabel = 'Username:';
    const usernametext = 'Enter user name';
    const passwordlabel = 'Password:';
    const buttontext = 'Log in';
    this.context.onSetTitle(title);
    this.context.onSetTitle(usernamelabel);
    this.context.onSetTitle(usernametext);
    this.context.onSetTitle(passwordlabel);
    this.context.onSetTitle(buttontext);

    return (
      <div className="LoginPage">
        <div className="LoginPage-container">
          <h1>{title}</h1>
          <section>
            <label htmlFor="username">{usernamelabel}</label>
            <input id="username" type="text" />
          </section>
          <section>
            <label htmlFor="password">{passwordlabel}</label>
            <input id="password" type="password" />
          </section>
          <a href="/register" onClick={Link.handleClick}>{buttontext}</a>
        </div>
      </div>
    );
  }
}

export default LoginPage;
